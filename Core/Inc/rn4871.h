/*
 * rn4871.h
 *
 *  Created on: Jan 11, 2021
 *      Author: axoul
 */

#ifndef INC_RN4871_H_
#define INC_RN4871_H_

#include "ringdma.h"
#include <stm32f3xx_hal.h>

#define RN4871_RX_BUFFER_SIZE 500
#define RN4871_MAX_RX_LINE_LENGTH 100

#define RN4871_PRIVATE_UUID_LENGTH_BITS       128
#define RN4871_PRIVATE_UUID_LENGTH_BYTES      (128 / 8)
#define RN4871_PRIVATE_UUID_HEX_STRING_LENGTH (RN4871_PRIVATE_UUID_LENGTH_BYTES * 2)
#define RN4871_MAX_UUID_LEN_BYTES             (128 / 8)

#ifndef RN4871_TIMEOUT
#define RN4871_TIMEOUT 2000
#endif

#ifndef RN4871_LOOKUP_TABLE_SIZE
#define RN4871_LOOKUP_TABLE_SIZE 20
#endif

typedef enum {
	RN4871_STATE_INITIALIZING,
	RN4871_STATE_READY,
	RN4871_STATE_WAITING_FOR_CMD,
	RN4871_STATE_WAITING_FOR_AOK,
	RN4871_STATE_WAITING_FOR_REBOOTING,
	RN4871_STATE_WAITING_FOR_REBOOT,
	RN4871_STATE_WAITING_FOR_RESET,
	RN4871_STATE_WAITING_FOR_END,
	RN4871_STATE_WAITING_FOR_LS
} RN4871_State;

typedef struct {
	uint16_t handle;
	uint8_t characteristicUUID[RN4871_MAX_UUID_LEN_BYTES];
	uint8_t characteristicUUIDLength;
} RN4871_handleItem;

typedef struct {
	UART_HandleTypeDef* ble_uart;
	GPIO_TypeDef* gpioPort;
	uint16_t gpioPin;

	volatile RN4871_State state;
	volatile bool connected;
	RingDma rxRing;
	uint8_t rxBuffer[RN4871_RX_BUFFER_SIZE];

	RN4871_handleItem handle[RN4871_LOOKUP_TABLE_SIZE];
	uint16_t handleLength;
} RN4871;

HAL_StatusTypeDef RN4871_setup(RN4871* rn4871);
HAL_StatusTypeDef RN4871_enter_cmd_Mode(RN4871* rn4871);
HAL_StatusTypeDef RN4871_wait_for_cmd(RN4871* rn4871);
HAL_StatusTypeDef RN4871_exit_cmd_Mode(RN4871* rn4871);
bool RN4871_isConnected(RN4871* rn4871);
void RN4871_tick(RN4871* rn4871);
void RN4871_send(RN4871* rn4871, const char* line);
HAL_StatusTypeDef RN4871_reboot(RN4871* rn4871);
HAL_StatusTypeDef RN4871_wait_for_reboot(RN4871* rn4871);
HAL_StatusTypeDef RN4871_setDeviceName(RN4871* rn4871, const char* deviceName);
HAL_StatusTypeDef RN4871_delimiter_reboot(RN4871* rn4871);
HAL_StatusTypeDef RN4871_clearServices(RN4871* rn4871);
HAL_StatusTypeDef RN4871_updateHandle(RN4871* rn4871);
HAL_StatusTypeDef RN4871_addPrivateService(RN4871* rn4871, const uint8_t* uuid);
HAL_StatusTypeDef RN4871_addPrivateCharacteristic(RN4871* rn4871, const uint8_t* uuid, uint8_t propertyOptions, uint8_t size);
void RN4871_uuidToString(char* dest, const uint8_t* uuid, uint8_t uuidLength);

#endif /* INC_RN4871_H_ */
