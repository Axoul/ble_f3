/*
 * ringdma.h
 *
 *  Created on: Jan 11, 2021
 *      Author: axoul
 */

#ifndef INC_RINGDMA_H_
#define INC_RINGDMA_H_

#include <stdbool.h>
#include <stdint.h>
#include <stm32f3xx_hal.h>

#ifndef min
#define min(a,b) ( ((a) < (b)) ? (a) : (b) )
#endif

typedef struct {
	volatile uint8_t* buffer;
	uint16_t size;
	volatile uint8_t* tailPtr;
	DMA_HandleTypeDef* dmaHandle;
} RingDma;

void RingDma_initUSARTRx(RingDma* ring, UART_HandleTypeDef* husart, uint8_t* buffer, uint16_t size);
uint16_t RingDma_available(RingDma* ring);
bool RingDma_readLine(RingDma* ring, char* line, uint16_t size);
uint8_t RingDma_read(RingDma* ring);
void strTrimRight(char* str);
int isWhitespace(char ch);

#endif /* INC_RINGDMA_H_ */
