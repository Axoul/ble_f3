/*
 * rn4871.c
 *
 *  Created on: Jan 11, 2021
 *      Author: axoul
 */

#include "rn4871.h"
#include "ringdma.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void _RN4871_setState(RN4871* rn4871, RN4871_State newState);
void _RN4871_setConnected(RN4871* rn4871, bool connected);
HAL_StatusTypeDef _RN4871_waitForReadyState(RN4871* rn4871);
void _RN4871_processLine(RN4871* rn4871, const char* line);
HAL_StatusTypeDef _RN4871_runAOKCommand(RN4871* rn4871, const char* cmd);
bool _RN4871_parseHandleUUIDLine(const char* line, RN4871_handleItem* handleItem);
void _RN4871_parseUUIDString(const char* str, uint8_t strLen, uint8_t* uuid, uint8_t* uuidLen);

/*
 * Public functions
 */

HAL_StatusTypeDef RN4871_setup(RN4871* rn4871) {
	RingDma_initUSARTRx(&rn4871->rxRing, rn4871->ble_uart, rn4871->rxBuffer, RN4871_RX_BUFFER_SIZE);
	rn4871->connected = false;
	_RN4871_setState(rn4871, RN4871_STATE_INITIALIZING);
	HAL_Delay(100);
	return RN4871_enter_cmd_Mode(rn4871);
}


HAL_StatusTypeDef RN4871_enter_cmd_Mode(RN4871* rn4871) {
	_RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_CMD);
	char* cmd_mode = "$$$";
	HAL_UART_Transmit(rn4871->ble_uart, (uint8_t*)cmd_mode, strlen(cmd_mode), RN4871_TIMEOUT);
	return _RN4871_waitForReadyState(rn4871);
}

HAL_StatusTypeDef RN4871_wait_for_cmd(RN4871* rn4871) {
	_RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_CMD);
	return _RN4871_waitForReadyState(rn4871);
}

HAL_StatusTypeDef RN4871_exit_cmd_Mode(RN4871* rn4871) {
	_RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_END);
	char* exit_cmd_mode = "---";
	HAL_UART_Transmit(rn4871->ble_uart, (uint8_t*)exit_cmd_mode, strlen(exit_cmd_mode), RN4871_TIMEOUT);
	return _RN4871_waitForReadyState(rn4871);
}

bool RN4871_isConnected(RN4871* rn4871) {
	return rn4871->connected;
}

void RN4871_tick(RN4871* rn4871) {
	char line[RN4871_MAX_RX_LINE_LENGTH];
	if (RingDma_readLine(&rn4871->rxRing, line, sizeof(line)) > 0) {
		strTrimRight(line);
		if (strlen(line) > 0) {
			_RN4871_processLine(rn4871, line);
		}
	}
}

void RN4871_send(RN4871* rn4871, const char* line) {
  char newLineCh = '\r';
  HAL_UART_Transmit(rn4871->ble_uart, (uint8_t*)line, strlen(line), RN4871_TIMEOUT);
  HAL_UART_Transmit(rn4871->ble_uart, (uint8_t*)&newLineCh, 1, RN4871_TIMEOUT);
}

HAL_StatusTypeDef RN4871_reboot(RN4871* rn4871) {
	_RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_REBOOTING);
	RN4871_send(rn4871, "R,1");
	return _RN4871_waitForReadyState(rn4871);
}

HAL_StatusTypeDef RN4871_wait_for_reboot(RN4871* rn4871) {
	_RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_REBOOT);
	return _RN4871_waitForReadyState(rn4871);
}

HAL_StatusTypeDef RN4871_setDeviceName(RN4871* rn4871, const char* deviceName) {
	char line[15];
	sprintf(line, "S-,%s", deviceName);
	return _RN4871_runAOKCommand(rn4871, line);
}

HAL_StatusTypeDef RN4871_delimiter_reboot(RN4871* rn4871) {
	return _RN4871_runAOKCommand(rn4871, "S%,%,#");
}

HAL_StatusTypeDef RN4871_clearServices(RN4871* rn4871) {
	return _RN4871_runAOKCommand(rn4871, "PZ");
}

HAL_StatusTypeDef RN4871_updateHandle(RN4871* rn4871) {
	rn4871->handleLength = 0;
	_RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_LS);
	RN4871_send(rn4871, "LS");
	return _RN4871_waitForReadyState(rn4871);
}

HAL_StatusTypeDef RN4871_addPrivateService(RN4871* rn4871, const uint8_t* uuid) {
	char line[50];
	strcpy(line, "PS,");
	RN4871_uuidToString(line + 3, uuid, RN4871_PRIVATE_UUID_LENGTH_BYTES);
	return _RN4871_runAOKCommand(rn4871, line);
}

HAL_StatusTypeDef RN4871_addPrivateCharacteristic(RN4871* rn4871, const uint8_t* uuid, uint8_t propertyOptions, uint8_t size) {
	char line[50];
	char* dest;
	strcpy(line, "PC,");
	RN4871_uuidToString(line + 3, uuid, RN4871_PRIVATE_UUID_LENGTH_BYTES);
	dest = line + strlen(line);
	sprintf(dest, ",%02X,%02X", propertyOptions, size);
	return _RN4871_runAOKCommand(rn4871, line);
}

void RN4871_uuidToString(char* dest, const uint8_t* uuid, uint8_t uuidLength) {
	for (int i = 0; i < uuidLength; i++) {
		sprintf(dest, "%02X", uuid[i]);
		dest += 2;
	}
}

/*
 * Private functions
 */

void _RN4871_setState(RN4871* rn4871, RN4871_State newState) {
	rn4871->state = newState;
}

void _RN4871_setConnected(RN4871* rn4871, bool connected) {
	rn4871->connected = connected;
}

HAL_StatusTypeDef _RN4871_waitForReadyState(RN4871* rn4871) {
	if(rn4871->state == RN4871_STATE_READY) {
		return HAL_OK;
	}

	uint32_t startTime = HAL_GetTick();
	while(1) {
		if(rn4871->state == RN4871_STATE_READY) {
			return HAL_OK;
		}
		if((HAL_GetTick() - startTime) > RN4871_TIMEOUT) {
			return HAL_TIMEOUT;
		}
		RN4871_tick(rn4871);
	}
}

void _RN4871_processLine(RN4871* rn4871, const char* line) {
	const static char* connect = "%CONNECT";
	const static char* disconnect = "%DISCONNECT";
	if(strncmp(line, connect, strlen(connect))) {
		_RN4871_setConnected(rn4871, true);
	}

	if(strncmp(line, disconnect, strlen(disconnect))) {
		_RN4871_setConnected(rn4871, false);
	}

	switch(rn4871->state) {
	case RN4871_STATE_INITIALIZING:
		break;

	case RN4871_STATE_WAITING_FOR_CMD:
		if((strcmp(line, "CMD") == 0) || (strcmp(line, "CMD>") == 0)) {
			_RN4871_setState(rn4871, RN4871_STATE_READY);
			return;
		}
		break;

	case RN4871_STATE_WAITING_FOR_AOK:
		if(strcmp(line, "AOK") == 0) {
			_RN4871_setState(rn4871, RN4871_STATE_READY);
			return;
		}
		break;

	case RN4871_STATE_WAITING_FOR_REBOOTING:
		if(strcmp(line, "Rebooting") == 0) {
			_RN4871_setState(rn4871, RN4871_STATE_READY);
			return;
		}

	case RN4871_STATE_WAITING_FOR_REBOOT:
		if(strcmp(line, "%REBOOT") == 0) {
			_RN4871_setState(rn4871, RN4871_STATE_READY);
			return;
		}
		break;

	case RN4871_STATE_WAITING_FOR_RESET:
		break;

	case RN4871_STATE_WAITING_FOR_END:
		if(strcmp(line, "END") == 0) {
			_RN4871_setState(rn4871, RN4871_STATE_READY);
			return;
		}
		break;

	case RN4871_STATE_WAITING_FOR_LS:
		if(_RN4871_parseHandleUUIDLine(line, &rn4871->handle[rn4871->handleLength])) {
			rn4871->handleLength++;
			return;
		}
		else if(strcmp(line, "END") == 0) {
			_RN4871_setState(rn4871, RN4871_STATE_READY);
		}
		else if(strlen(line) == 4 || strlen(line) == RN4871_PRIVATE_UUID_HEX_STRING_LENGTH) {
			return;
		}
		break;

	case RN4871_STATE_READY:
		break;

	}
}

HAL_StatusTypeDef _RN4871_runAOKCommand(RN4871* rn4871, const char* cmd) {
  _RN4871_setState(rn4871, RN4871_STATE_WAITING_FOR_AOK);
  RN4871_send(rn4871, cmd);
  return _RN4871_waitForReadyState(rn4871);
}


bool _RN4871_parseHandleUUIDLine(const char* line, RN4871_handleItem* handleItem) {
	if(strncmp(line, "  ", 2) != 0) {
		return false;
	}
	const char* startOfUUIDPtr = line + 2;
	const char* firstCommaPtr = strchr(startOfUUIDPtr, ',');
	_RN4871_parseUUIDString(startOfUUIDPtr, firstCommaPtr - startOfUUIDPtr, handleItem->characteristicUUID, &handleItem->characteristicUUIDLength);

	char handleStr[5];
	strncpy(handleStr, firstCommaPtr + 1, 4);
	handleStr[4] = 0;
	handleItem->handle = strtol(handleStr, NULL, 16);
	return true;
}

void _RN4871_parseUUIDString(const char* str, uint8_t strLen, uint8_t* uuid, uint8_t* uuidLen) {
	char temp[3];
	uint8_t strIndex, destIndex;
	for (strIndex = 0, destIndex = 0; strIndex < strLen; strIndex += 2) {
		temp[0] = str[strIndex];
		temp[1] = str[strIndex + 1];
		temp[2] = '\0';
		uuid[destIndex++] = strtol(temp, NULL, 16);
	}
	*uuidLen = destIndex;
}
